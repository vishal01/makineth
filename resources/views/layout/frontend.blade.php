<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="MACKINAT | Search Engine for Machines.">
    <meta name="author" content="Daddy Cool">
    <title>MACKINAT | Search Engine for Machines.</title>
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Montserrat:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">

    <!-- COMMON CSS -->
    <link href="{{ asset('Assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('Assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('Assets/css/vendors.css') }}" rel="stylesheet">
    <link href="{{ asset('Assets/css/bmenu.css') }}" rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link href="{{ asset('Assets/css/custom.css') }}" rel="stylesheet">
</head>
<body>
    <div id="preloader">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- End Preload -->
    <div class="layer"></div>
    <header id="plain">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div id="logo_home">
                        <h1><a href="index.html" title="MACKINAT">MACKINAT</a></h1>
                    </div>
                </div>
                @include('layout.menu')
            </div>
        </div><!-- container -->
    </header><!-- End Header -->
    @yield('content')
    <!-- End main revealed-->
    <footer class="plus_border">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3 data-target="#collapse_ft_1">Quick Links</h3>
                    <ul class="links">
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Help</a></li>
                        <li><a href="#">My account</a></li>
                        <li><a href="#">Create account</a></li>
                        <li><a href="#">Contacts</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3 data-target="#collapse_ft_2">Learn more</h3>
                    <ul>
                        <li><a href="#">Mackinat Assurance</a></li>
                        <li><a href="#">International Suppliers</a></li>
                        <li><a href="#">What makes us different</a></li>
                        <li><a href="#">Service & Support</a></li>
                        <li><a href="#">Pricacy Policy</a></li>
                        <li><a href="#">Market Trends</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3 data-target="#collapse_ft_3">Contacts</h3>
                    <ul>
                        <li><i class="ti-home"></i>97845 Baker st. 567<br>Dubai - UAE</li>
                        <li><i class="ti-headphone-alt"></i>+971 545 28 7463</li>
                        <li><i class="ti-email"></i><a href="#0">info@mackinat.com</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3>Discover now</h3>
                    <div id="newsletter">
                        <div id="message-newsletter"></div>
                        <form method="post" action="#" name="newsletter_form" id="newsletter_form">
                            <div class="form-group">
                                <input type="email" name="email_newsletter" id="email_newsletter" class="form-control" placeholder="Your email">
                                <p></p>
                                <p class=" add_bottom_30">
                                    <a href="#" class="btn_1">Subscribe</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="social_footer">
                        <ul>
                            <li><a href="#"><i class="icon-facebook"></i></a></li>
                            <li><a href="#"><i class="icon-twitter"></i></a></li>
                            <li><a href="#"><i class="icon-google"></i></a></li>
                            <li><a href="#"><i class="icon-instagram"></i></a></li>
                            <li><a href="#"><i class="icon-pinterest"></i></a></li>
                            <li><a href="#"><i class="icon-vimeo"></i></a></li>
                            <li><a href="#"><i class="icon-youtube-play"></i></a></li>
                        </ul>
                        <p>© Mackinat 2020. All rights reserved. Powered by DC.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End footer -->
    <div id="toTop"></div><!-- Back to top button -->
    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit">
                <i class="icon_set_1_icon-78"></i>
            </button>
        </form>
    </div><!-- End Search Menu -->
    <!-- Sign In Popup -->
    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">
        <div class="small-dialog-header">
            <h3>Sign In</h3>
        </div>
        <form>
            <div class="sign-in-wrapper">
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" id="email">
                    <i class="icon_mail_alt"></i>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" id="password" value="">
                    <i class="icon_lock_alt"></i>
                </div>
                <div class="form-group">
                    <label>User Role</label>
                    <label class="switch-light switch-ios pull-right">
                        <input type="checkbox" name="option_1" id="option_1" checked value="">
                        <span>
                            <span>Buyer</span>
                            <span>Seller</span>
                        </span>
                        <a></a>
                    </label>
                </div>
                <div class="clearfix add_bottom_15">
                    <div class="checkboxes float-left">
                        <input id="remember-me" type="checkbox" name="check">
                        <label for="remember-me">Remember Me</label>
                    </div>
                    <div class="float-right"><a id="forgot" href="javascript:void(0);">Forgot Password?</a></div>
                </div>
                <div class="text-center"><input type="submit" value="Log In" class="btn_login"></div>
                <div class="text-center">
                    Don’t have an account? <a href="javascript:void(0);">Sign up</a>
                </div>
                <div id="forgot_pw">
                    <div class="form-group">
                        <label>Please confirm login email below</label>
                        <input type="email" class="form-control" name="email_forgot" id="email_forgot">
                        <i class="icon_mail_alt"></i>
                    </div>
                    <p>You will receive an email containing a link allowing you to reset your password to a new preferred one.</p>
                    <div class="text-center"><input type="submit" value="Reset Password" class="btn_1"></div>
                </div>
            </div>
        </form>
        <!--form -->
    </div>
    <!-- /Sign In Popup -->
    <!-- Common scripts -->
    <script src="{{ asset('Assets/js/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ asset('Assets/js/common_scripts_min.js') }}"></script>
    <script src="{{ asset('Assets/js/functions.js') }}"></script>
    <script src="{{ asset('Assets/js/owl.carousel.js') }}"></script>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->
    <script type="text/javascript">
        $({ Counter: 0 }).animate({
            Counter: $('.Single1').text()
        }, {
            duration: 8000,
            easing: 'swing',
            step: function () {
                $('.Single1').text(Math.ceil(this.Counter));
            }
        });
    </script>

    <script type="text/javascript">
        $({ Counter: 0 }).animate({
            Counter: $('.Single2').text()
        }, {
            duration: 8000,
            easing: 'swing',
            step: function () {
                $('.Single2').text(Math.ceil(this.Counter));
            }
        });
    </script>

    <script type="text/javascript">
        $({ Counter: 0 }).animate({
            Counter: $('.Single3').text()
        }, {
            duration: 5000,
            easing: 'swing',
            step: function () {
                $('.Single3').text(Math.ceil(this.Counter));
            }
        });
    </script>
</body>

</html >
