<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" dir="{{ __('voyager::generic.is_rtl') == 'true' ? 'rtl' : 'ltr' }}">
<head>
    <title>@yield('page_title', setting('admin.title') . " - " . setting('admin.description'))</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="assets-path" content="{{ route('voyager.voyager_assets') }}"/>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

    <!-- Favicon -->
    <?php $admin_favicon = Voyager::setting('admin.icon_image', ''); ?>
    @if($admin_favicon == '')
        <link rel="shortcut icon" href="{{ voyager_asset('images/logo-icon.png') }}" type="image/png">
    @else
        <link rel="shortcut icon" href="{{ Voyager::image($admin_favicon) }}" type="image/png">
    @endif



    <!-- App CSS -->
    <link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">

    @yield('css')
    @if(__('voyager::generic.is_rtl') == 'true')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
        <link rel="stylesheet" href="{{ voyager_asset('css/rtl.css') }}">
    @endif

    <!-- Few Dynamic Styles -->
    <style type="text/css">
        .voyager .side-menu .navbar-header {
            background:{{ config('voyager.primary_color','#22A7F0') }};
            border-color:{{ config('voyager.primary_color','#22A7F0') }};
        }
        .widget .btn-primary{
            border-color:{{ config('voyager.primary_color','#22A7F0') }};
        }
        .widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus{
            background:{{ config('voyager.primary_color','#22A7F0') }};
        }
        .voyager .breadcrumb a{
            color:{{ config('voyager.primary_color','#22A7F0') }};
        }
    </style>

    @if(!empty(config('voyager.additional_css')))<!-- Additional CSS -->
        @foreach(config('voyager.additional_css') as $css)<link rel="stylesheet" type="text/css" href="{{ asset($css) }}">@endforeach
    @endif

    @yield('head')
</head>

<body class="voyager @if(isset($dataType) && isset($dataType->slug)){{ $dataType->slug }}@endif">

<div id="voyager-loader" style="display: none;">
    <?php $admin_loader_img = Voyager::setting('admin.loader', ''); ?>
    @if($admin_loader_img == '')
        <img src="{{ voyager_asset('images/logo-icon.png') }}" alt="Voyager Loader">
    @else
        <img src="{{ Voyager::image($admin_loader_img) }}" alt="Voyager Loader">
    @endif
</div>

<?php
if (\Illuminate\Support\Str::startsWith(Auth::user()->avatar, 'http://') || \Illuminate\Support\Str::startsWith(Auth::user()->avatar, 'https://')) {
    $user_avatar = Auth::user()->avatar;
} else {
    $user_avatar = Voyager::image(Auth::user()->avatar);
}
?>

<div class="app-container">
    <div class="fadetoblack visible-xs"></div>
    <div class="row content-container">
        @include('voyager::dashboard.navbar')
        <div class="side-menu sidebar-inverse ps ps--theme_default ps--active-x" style="" data-ps-id="5eca2d55-5458-12f4-e617-20acf4619d54">
   <nav class="navbar navbar-default" role="navigation">
      <div class="side-menu-container">
         <div class="navbar-header">
            <a class="navbar-brand" href="http://localhost/makineth/admin">
               <div class="logo-icon-container">
                  <img src="http://localhost/makineth/admin/voyager-assets?path=images%2Flogo-icon-light.png" alt="Logo Icon">
               </div>
               <div class="title">Voyager</div>
            </a>
         </div>
         <!-- .navbar-header -->
         <div class="panel widget center bgimage" style="background-image:url(http://localhost/makineth/admin/voyager-assets?path=images%2Fbg.jpg); background-size: cover; background-position: 0px;">
            <div class="dimmer"></div>
            <div class="panel-content">
               <img src="http://localhost/makineth/public/storage/users/default.png" class="avatar" alt="Admin avatar">
               <h4>Admin</h4>
               <p>admin@admin.com</p>
               <a href="http://localhost/makineth/admin/profile" class="btn btn-primary">Profile</a>
               <div style="clear:both"></div>
            </div>
         </div>
      </div>
      <div id="adminmenu">
         <ul class="nav navbar-nav">
            <li class="active">
               <a target="_self" href="http://localhost/makineth/admin"><span class="icon voyager-boat"></span> <span class="title">Dashboard</span></a> <!---->
            </li>
            <li class="">
               <a target="_self" href="http://localhost/makineth/admin/roles"><span class="icon voyager-lock"></span> <span class="title">Roles</span></a> <!---->
            </li>
            <li class="">
               <a target="_self" href="http://localhost/makineth/admin/users"><span class="icon voyager-person"></span> <span class="title">Users</span></a> <!---->
            </li>
            <li class="dropdown">
               <a target="_self" href="#18-dropdown-element" data-toggle="collapse" aria-expanded="false"><span class="icon voyager-news"></span> <span class="title">CMS</span></a> 
               <div id="18-dropdown-element" class="panel-collapse collapse ">
                  <div class="panel-body">
                     <ul class="nav navbar-nav">
                        <li class="">
                           <a target="_self" href="http://localhost/makineth/admin/categories"><span class="icon voyager-categories"></span> <span class="title">Categories</span></a> <!---->
                        </li>
                        <li class="">
                           <a target="_self" href="http://localhost/makineth/admin/pages"><span class="icon voyager-file-text"></span> <span class="title">Pages</span></a> <!---->
                        </li>
                        <li class="">
                           <a target="_self" href="http://localhost/makineth/admin/posts"><span class="icon voyager-news"></span> <span class="title">Posts</span></a> <!---->
                        </li>
                     </ul>
                  </div>
               </div>
            </li>
            <li class="dropdown active">
               <a target="_self" href="#5-dropdown-element" data-toggle="collapse" aria-expanded="true"><span class="icon voyager-tools"></span> <span class="title">Tools</span></a> 
               <div id="5-dropdown-element" class="panel-collapse collapse ">
                  <div class="panel-body">
                     <ul class="nav navbar-nav">
                        <li class="">
                           <a target="_self" href="http://localhost/makineth/admin/menus"><span class="icon voyager-list"></span> <span class="title">Menu Builder</span></a> <!---->
                        </li>
                        <li class="active">
                           <a target="_self" href="http://localhost/makineth/admin/languages"><span class="icon voyager-code"></span> <span class="title">Languages</span></a> <!---->
                        </li>
                     </ul>
                  </div>
               </div>
            </li>
            <li class="">
               <a target="_self" href="http://localhost/makineth/admin/settings"><span class="icon voyager-settings"></span> <span class="title">Settings</span></a> <!---->
            </li>
         </ul>
      </div>
   </nav>
   <div class="ps__scrollbar-x-rail" style="width: 60px; left: 0px; bottom: 0px;">
      <div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 14px;"></div>
   </div>
   <div class="ps__scrollbar-y-rail" style="top: 0px; right: 0px;">
      <div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
   </div>
</div>
        <script>
            (function(){
                    var appContainer = document.querySelector('.app-container'),
                        sidebar = appContainer.querySelector('.side-menu'),
                        navbar = appContainer.querySelector('nav.navbar.navbar-top'),
                        loader = document.getElementById('voyager-loader'),
                        hamburgerMenu = document.querySelector('.hamburger'),
                        sidebarTransition = sidebar.style.transition,
                        navbarTransition = navbar.style.transition,
                        containerTransition = appContainer.style.transition;

                    sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition =
                    appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition =
                    navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = 'none';

                    if (window.innerWidth > 768 && window.localStorage && window.localStorage['voyager.stickySidebar'] == 'true') {
                        appContainer.className += ' expanded no-animation';
                        loader.style.left = (sidebar.clientWidth/2)+'px';
                        hamburgerMenu.className += ' is-active no-animation';
                    }

                   navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = navbarTransition;
                   sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition = sidebarTransition;
                   appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition = containerTransition;
            })();
        </script>
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="side-body padding-top">
                @yield('page_header')
                <div id="voyager-notifications"></div>
                @yield('content')
            </div>
        </div>
    </div>
</div>
@include('voyager::partials.app-footer')

<!-- Javascript Libs -->


<script type="text/javascript" src="{{ voyager_asset('js/admin.js') }}"></script>

<script>
    @if(Session::has('alerts'))
        let alerts = {!! json_encode(Session::get('alerts')) !!};
        helpers.displayAlerts(alerts, toastr);
    @endif

    @if(Session::has('message'))

    // TODO: change Controllers to use AlertsMessages trait... then remove this
    var alertType = {!! json_encode(Session::get('alert-type', 'info')) !!};
    var alertMessage = {!! json_encode(Session::get('message')) !!};
    var alerter = toastr[alertType];

    if (alerter) {
        alerter(alertMessage);
    } else {
        toastr.error("toastr alert-type " + alertType + " is unknown");
    }
    @endif
</script>
@include('voyager::media.manager')
@yield('javascript')
@stack('javascript')
@if(!empty(config('voyager.additional_js')))<!-- Additional Javascript -->
    @foreach(config('voyager.additional_js') as $js)<script type="text/javascript" src="{{ asset($js) }}"></script>@endforeach
@endif
<style type="text/css">
.app-container.expanded .app-footer {
    left: 0px !important;
}
</style>
</body>
</html>
