@extends('layout.frontend')
@section('content')
@php
$lang = Config::get('app.locale'); 
$bannertitle = App\Model\Translations::where('locale','=',$lang)
            ->where('column_name','=','title')
            ->first(); 

$bannerdescr = App\Model\Translations::where('locale','=',$lang)
            ->where('column_name','=','description')
            ->first();

@endphp 
        <section id="hero" class="subheader_plain" style="background: #4d536d url({{ Voyager::image( $home->image ) }}) no-repeat center center;">
        <div class="intro_title">
            <h3 style="display: table;margin: 0px auto 0px auto; padding:5px;font-size:20px; text-shadow: 3px 2px #000000;">
            @if($lang != 'en')
            {{$bannertitle->value }}
            @else
            {{ $home->title }}
            @endif
            </h3>
            <p style="display: table;margin: 0px auto 0px auto; padding:5px;font-size:20px; text-shadow: 3px 2px #000000;">
            @if($lang != 'en')
            {{$bannerdescr->value }}
            @else
            {{ $home->description }}
            @endif
            </p>
            <form method="post" action="#">
                <div class="row no-gutters custom-search-input-2">

                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Search for any machine around the world...">
                            <i class="icon_search"></i>
                        </div>
                    </div>
                    <div class="col-lg-2" style="text-align:left;"> <input type="submit" style="background-color:#3b4a52;width:40%;" value="Search"></div>
                    <div class="col-lg-2"></div>
                </div>
                <!-- /row -->
            </form>
        </div>
    </section>
    <!-- End section -->
    <main>
        <div class="container margin_60">

            <div class="main_title">
                <h2> Featured Products</h2>
                <p>Browse through the featured products in Mackinat. Make your procurement hassle-free.</p>
            </div>

            <div class="owl-carousel owl-theme list_carousel add_bottom_30">
                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="product-details.html">
                                <img src="{{ asset('Assets/img/prod1.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product one</strong></h3>
                            <div class="price">
                                AED. 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->
                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="product-details.html">
                                <img src="{{ asset('Assets/img/prod2.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                                <!--<div class="short_info">
                                    Product two<span class="price"><sup>ask</sup>price</span>

                                </div>-->
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product two</strong></h3>
                            <div class="price">
                                AED 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->
                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="product-details.html">
                                <img src="{{ asset('Assets/img/prod3.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                                <div class="badge_save">Save<strong>30%</strong></div>
                                <!--<div class="short_info">
                                    Product three <span class="price"><sup>AED</sup>25000</span>

                                </div>-->
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product three</strong></h3>
                            <div class="price">
                                AED 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->
                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="product-details.html">
                                <img src="{{ asset('Assets/img/prod4.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                                <div class="badge_save">Save<strong>20%</strong></div>
                                <!--<div class="short_info">
                                    Product four<span class="price"><sup>ask</sup>price</span>
                                </div>-->
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product four</strong></h3>
                            <div class="price">
                                AED 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->


                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="product-details.html">
                                <img src="{{ asset('Assets/img/prod1.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                                <!--<div class="short_info">
                                    Product one<span class="price"><sup>AED</sup>25000</span>
                                </div>-->
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product five</strong></h3>
                            <div class="price">
                                AED 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->
                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="product-details.html">
                                <img src="{{ asset('Assets/img/prod2.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                                <!--<div class="short_info">
                                    Product two<span class="price"><sup>ask</sup>price</span>

                                </div>-->
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product three</strong></h3>
                            <div class="price">
                                AED 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->
                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="#product-details.html">
                                <img src="{{ asset('Assets/img/prod3.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                                <div class="badge_save">Save<strong>30%</strong></div>
                                <!--<div class="short_info">
                Product three <span class="price"><sup>AED</sup>25000</span>

            </div>-->
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product five</strong></h3>
                            <div class="price">
                                AED 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->
                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="product-details.html">
                                <img src="{{ asset('Assets/img/prod4.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                                <div class="badge_save">Save<strong>20%</strong></div>
                                <!--<div class="short_info">
                Product four<span class="price"><sup>ask</sup>price</span>
            </div>-->
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product five</strong></h3>
                            <div class="price">
                                AED 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->



                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="product-details.html">
                                <img src="{{ asset('Assets/img/prod1.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                                <!--<div class="short_info">
                                    Product one<span class="price"><sup>AED</sup>25000</span>
                                </div>-->
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product five</strong></h3>
                            <div class="price">
                                AED 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->
                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="product-details.html">
                                <img src="{{ asset('Assets/img/prod2.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                                <!--<div class="short_info">
                                    Product two<span class="price"><sup>ask</sup>price</span>

                                </div>-->
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product five</strong></h3>
                            <div class="price">
                                AED 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->
                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="product-details.html">
                                <img src="{{ asset('Assets/img/prod3.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                                <div class="badge_save">Save<strong>30%</strong></div>
                                <!--<div class="short_info">
                Product three <span class="price"><sup>AED</sup>25000</span>

            </div>-->
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product five</strong></h3>
                            <div class="price">
                                AED 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->
                <div class="item">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Featured</span></div>
                        <div class="img_container">
                            <a href="#product-details.html">
                                <img src="{{ asset('Assets/img/prod4.jpg') }}" width="800" height="533" class="img-fluid" alt="image">
                                <div class="badge_save">Save<strong>20%</strong></div>
                                <!--<div class="short_info">
                                    Product four<span class="price"><sup>ask</sup>price</span>
                                </div>-->
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Product five</strong></h3>
                            <div class="price">
                                AED 25000
                            </div>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                <!-- /item -->

            </div>
            <!-- /carousel -->

            <p class="text-center add_bottom_30">
                <a href="product-list.html" class="btn_1">VIEW ALL</a>
            </p>

            <!--<hr class="mt-5 mb-5">-->
        </div><!-- End container -->

        <div class="white_bg">
            <div class="container margin_60">
                <div class="main_title">

                    <p>
                        <!--Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.-->
                    </p>
                </div>

                <!-- End row -->
                <p style="font-size:smaller; text-align:end">Sponsored Ads </p>
                <div class="banner colored">
                    <h4>Discover our Top tours <span>from $34</span></h4>
                    <p>
                        Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                    </p>
                    <a href="#" class="btn_1 white">Read more</a>

                </div>


                <div class="row">
                    <div class="col-lg-3 col-md-6 text-center">
                        <p>
                            <a href="#"><img src="{{ asset('Assets/img/bus.jpg') }}" alt="Pic" class="img-fluid"></a>
                        </p>
                        <h4><a href="#" style="color:black">Ad Title</a></h4>
                        <p>
                            <a href="#" style="color:black">Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.</a>
                        </p>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <p>
                            <a href="#"><img src="{{ asset('Assets/img/transfer.jpg') }}" alt="Pic" class="img-fluid"></a>
                        </p>
                        <h4><a href="#" style="color:black">Ad Title</a></h4>
                        <p>
                            <a href="#" style="color:black">Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.</a>
                        </p>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <p>
                            <a href="#"><img src="{{ asset('Assets/img/guide.jpg') }}" alt="Pic" class="img-fluid"></a>
                        </p>
                        <h4><a href="#" style="color:black">Ad Title</a></h4>
                        <p>
                            <a href="#" style="color:black">Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.</a>
                        </p>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <p>
                            <a href="#"><img src="{{ asset('Assets/img/hotel.jpg') }}" alt="Pic" class="img-fluid"></a>
                        </p>
                        <h4><a href="#" style="color:black">Ad Title</a></h4>
                        <p>
                            <a href="#" style="color:black">Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.</a>
                        </p>
                    </div>
                </div>
                <!-- End row -->
            </div>
            <!-- End container -->
        </div>
        <!-- End white_bg -->
        <!-- End section -->
        <!--client-->
        <div class="container margin_60">
            <div class="container margin_80_55">
                <div class="main_title">
                    <h2>Our Clients </h2>
                    <p>
                        Major companies using Mackinat.
                    </p>
                </div>
                <div id="carousel" class="owl-carousel owl-theme list_carousel add_bottom_30">
                    <div class="item">
                        <a href="#0">
                            <img src="{{ asset('Assets/img/partner1.png') }}" height="115" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#0">
                            <img src="{{ asset('Assets/img/partner2.png') }}" height="115" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#0">
                            <img src="{{ asset('Assets/img/partner3.png') }}" height="115" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#0">
                            <img src="{{ asset('Assets/img/partner4.png') }}" height="115" alt="">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#0">
                            <img src="{{ asset('Assets/img/partner5.png') }}" height="115" alt="">
                        </a>
                    </div>
                </div>
                <!-- /carousel -->
            </div>
        </div>
        <!-- End row -->
        <!-- End container -->
        <!--why?-->
        <div class="white_bg">
            <div class="container margin_60">
                <div class="row small-gutters categories_grid">
                    <div class="col-sm-12 col-md-12">

                        <div class="main_title">
                            <h2>Why MACKINAT<!--<img src="img/logo_sticky.svg" width="160" height="38" alt="MACKINAT" data-retina="true">--> ? </h2>
                            <p>
                                <!--Major companies using Mackinat.-->
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <div class="row">
                            <div class="col-lg-4 wow zoomIn" data-wow-delay="0.2s">
                                <div class="feature_home">
                                    <i class="icon-users-3"></i>
                                    <h3><span class="Single1">1000</span>+ Sellers registered</h3>
                                </div>
                            </div>
                            <div class="col-lg-4 wow zoomIn" data-wow-delay="0.4s">
                                <div class="feature_home">
                                    <i class="icon-group"></i>
                                    <h3><span class="Single2">1000</span>+ Buyers registered</h3>
                                </div>
                            </div>
                            <div class="col-lg-4 wow zoomIn" data-wow-delay="0.6s">
                                <div class="feature_home">
                                    <i class="icon-basket-3"></i>
                                    <h3><span class="Single3">2000</span>+ Products listed</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <div class="main_title">
                            <a href="#">
                                <h6>Find out more about how mackinat works? </h6>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <div class="row small-gutters mt-md-0 mt-sm-2">
                            <div class="col-sm-6">
                                <a href="seller-signup.html">
                                    <img src="{{ asset('Assets/img/sellhome.jpg') }}" alt="" class="img-fluid">
                                </a>
                                <p>&nbsp;</p>
                                <p style="width: 100%; margin:auto; text-align: center !important;display:block;">
                                    Interested in being featured on Mackinat, connect with other buyers
                                </p>
                                <p>&nbsp;</p>
                                <p class="text-center add_bottom_30" style="width: 50%; margin:auto; text-align: center !important;display:block;">
                                    <a href="#" class="btn_1">Sign up as Seller</a>
                                </p>
                            </div>
                            
                            <div class="col-sm-6">
                                <a href="Buyer-signup.html">
                                    <img src="{{ asset('Assets/img/buyhome.jpg') }}" alt="" class="img-fluid">
                                </a>
                                <!--<h2> </h2>-->
                                <p>&nbsp;</p>
                                <p  style="width: 100%; margin:auto; text-align: center !important;display:block;">Stay up to date with the latest products and machines</p>
                                <p>&nbsp;</p>
                                <p class="text-center add_bottom_30"  style="width: 50%; margin:auto; text-align: center !important;display:block;">
                                    <a href="#" class="btn_1">Sign up as Buyer</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/categories_grid-->
            </div>
            <!-- /container -->
        </div>
    </main>
@endsection