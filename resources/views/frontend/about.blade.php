@extends('layout.frontend')

@section('content')
<main>
	@php
    $lang = Config::get('app.locale');
    $body = App\Model\Translations::where('locale','=',$lang)
            ->where('column_name','=','body')
            ->where('foreign_key','=',$about->id)
            ->first();
	@endphp
	@if($lang != 'en')
	{!! html_entity_decode($body->value) !!}
	@else
	{!! html_entity_decode($about->body) !!}
	
	@endif
        <div class="container margin_60">

            <div class="main_title">
                <h2>What customers says</h2>
                <p>Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.</p>
            </div>

            <div class="row">
                <div class="col-lg-6"> 
                    <div class="review_strip">
                        <img src="{{ asset('Assets/img/avatar1.jpg') }}" alt="Image" class="rounded-circle">
                        <h4>Company name</h4>
                        <p>
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
                        </p>
                    </div>
                    <!-- End review strip -->
                </div>

                <div class="col-lg-6">
                    <div class="review_strip">
                        <img src="{{ asset('Assets/img/avatar2.jpg') }}" alt="Image" class="rounded-circle">
                        <h4>Company name</h4>
                        <p>
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
                        </p>
                    </div>
                    <!-- End review strip -->
                </div>
            </div>
            <!-- End row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="review_strip">
                        <img src="{{ asset('Assets/img/avatar3.jpg') }}" alt="Image" class="rounded-circle">
                        <h4>Company name</h4>
                        <p>
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
                        </p>
                    </div>
                    <!-- End review strip -->
                </div>

                <div class="col-lg-6">
                    <div class="review_strip">
                        <img src="{{ asset('Assets/img/avatar1.jpg') }}" alt="Image" class="rounded-circle">
                        <h4>Company name</h4>
                        <p>
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus."
                        </p>
                    </div>
                    <!-- End review strip -->
                </div>
            </div>
        </div>
        <!-- End Container -->

@endsection