<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'Frontend\HomeController@index');
Route::get('about', 'Frontend\AboutController@index');

Route::group(['prefix' => 'Manage'], function () {
    Voyager::routes();
    Route::get('languages', 'Admin\LanguageTranslationController@index')->name('languages');
    Route::post('translations/update', 'Admin\LanguageTranslationController@transUpdate')->name('translation.update.json');
    Route::post('translations/updateKey', 'Admin\LanguageTranslationController@transUpdateKey')->name('translation.update.json.key');
    Route::delete('translations/destroy/{key}', 'Admin\LanguageTranslationController@destroy')->name('translations.destroy');
    Route::post('translations/create', 'Admin\LanguageTranslationController@store')->name('translations.create');
    Route::get('check-translation', function(){
        \App::setLocale('fr');

        dd(__('website'));
    });

    //Route::resource('banner', 'Admin\BannerController');

    
});



