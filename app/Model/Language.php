<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
     /*table name*/
    protected $table      = 'languages';  

    /*primarykey*/  
  	protected $primaryKey = 'id';

  	/*table fields*/
  	protected $fillable  = [
  						   'name',
                           'code',
                           'created_at',
                           'updated_at'
                           ];
}
