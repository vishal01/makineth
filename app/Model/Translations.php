<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Translations extends Model
{
     /*table name*/
    protected $table      = 'translations';  

    /*primarykey*/  
  	protected $primaryKey = 'id';

  	/*table fields*/
  	protected $fillable  = [
  						   'table_name',
                           'column_name',
                           'foreign_key',
                           'locale',
                           'value',
                           'created_at',
                           'updated_at'
                           ];
}
