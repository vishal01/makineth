<?php

namespace App\Model;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
	use Translatable;
     /*table name*/
    protected $table      = 'banner';  

    protected $translatable = ['title', 'description'];

    /*primarykey*/  
  	protected $primaryKey = 'id';

  	/*table fields*/
  	protected $fillable  = [
  						   'pageId',
                           'title',
                           'description',
                           'image',
                           'status',
                           'created_at',
                           'updated_at'
                           ];

    public function localeselect() {
       return $this->belongsTo('App\Model\Translations', 'id');
    }
}
