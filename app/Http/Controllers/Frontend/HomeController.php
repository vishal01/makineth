<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config;

use App\Model\Banner;

class HomeController extends Controller
{
    public function index()
    {
    	
    	$data['home'] = Banner::join('pages', 'banner.pageId', '=', 'pages.id')
    	                ->select('banner.id','banner.title','banner.description','banner.image')
    					->where('pages.slug','=','home')
    	                ->where('banner.status','=','ACTIVE')
    	                ->first(); 
    	return view('frontend/index',$data);
    }
}
