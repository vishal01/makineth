<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Page;

class AboutController extends Controller
{
    public function index()
    {
    	$data['about'] = Page::join('translations', 'pages.id', '=', 'translations.foreign_key') 
    	                 ->select('pages.id','pages.title','pages.body')
    	                 ->where('pages.slug','=','about')
    	                  ->where('pages.status','=','ACTIVE')
    	                  ->first();
    	return view('frontend/about',$data);
    }
}
