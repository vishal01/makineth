-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2020 at 12:51 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mrfixdubai_makineth`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pageId` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `pageId`, `title`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(6, 2, 'SEARCH ENGINE FOR MACHINES!', 'DISCOVER THE MACHINES AROUND THE WORLD', 'banner\\June2020\\vcm7JR2CU6YUvOR3cZx5.jpg', 'ACTIVE', '2020-06-30 02:47:52', '2020-06-30 02:49:03');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2020-06-27 04:39:34', '2020-06-27 04:39:34'),
(2, NULL, 1, 'Category 2', 'category-2', '2020-06-27 04:39:34', '2020-06-27 04:39:34');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(90, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 2),
(91, 24, 'pageId', 'text', 'PageId', 0, 1, 1, 1, 1, 1, '{}', 3),
(92, 24, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 4),
(93, 24, 'description', 'text_area', 'Description', 1, 1, 1, 1, 1, 1, '{}', 5),
(94, 24, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 6),
(95, 24, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 7),
(96, 24, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 8),
(97, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(99, 24, 'banner_belongsto_page_relationship', 'relationship', 'pages', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Model\\\\Page\",\"table\":\"pages\",\"type\":\"belongsTo\",\"column\":\"pageId\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"banner\",\"pivot\":\"0\",\"taggable\":\"0\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-06-27 04:39:20', '2020-06-27 04:39:20'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-06-27 04:39:20', '2020-06-27 04:39:20'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-06-27 04:39:20', '2020-06-27 04:39:20'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2020-06-27 04:39:33', '2020-06-27 04:39:33'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2020-06-27 04:39:34', '2020-06-27 04:39:34'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2020-06-27 04:39:36', '2020-06-27 04:39:36'),
(24, 'banner', 'banner', 'Banner', 'Banner', 'voyager-images', 'App\\Model\\Banner', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-06-30 00:12:19', '2020-06-30 00:49:02');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', NULL, NULL),
(2, 'Arabic', 'ar', NULL, NULL),
(3, 'Franch', 'fr', NULL, NULL),
(4, 'German', 'de', NULL, NULL),
(5, 'Italy', 'it', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-06-27 04:39:22', '2020-06-27 04:39:22'),
(2, 'user', '2020-06-30 03:38:41', '2020-06-30 03:38:41');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', '#000000', NULL, 1, '2020-06-27 04:39:22', '2020-06-29 04:42:08', 'voyager.dashboard', 'null'),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2020-06-27 04:39:22', '2020-06-28 23:04:44', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-06-27 04:39:22', '2020-06-27 04:39:22', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-06-27 04:39:22', '2020-06-27 04:39:22', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 6, '2020-06-27 04:39:22', '2020-06-29 03:24:48', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-06-27 04:39:22', '2020-06-29 23:07:46', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 3, '2020-06-27 04:39:22', '2020-06-29 23:07:46', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 4, '2020-06-27 04:39:22', '2020-06-29 23:07:46', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 5, '2020-06-27 04:39:23', '2020-06-29 23:07:46', 'voyager.bread.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, 18, 1, '2020-06-27 04:39:33', '2020-06-29 03:24:51', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, 18, 3, '2020-06-27 04:39:35', '2020-06-29 03:24:51', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, 18, 2, '2020-06-27 04:39:37', '2020-06-29 03:24:51', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 6, '2020-06-27 04:39:41', '2020-06-29 23:07:46', 'voyager.hooks', NULL),
(17, 1, 'Languages', 'Manage/languages', '_self', 'voyager-code', '#000000', 5, 2, '2020-06-28 22:58:10', '2020-06-29 23:07:46', NULL, ''),
(18, 1, 'CMS', '', '_self', 'voyager-news', '#000000', NULL, 5, '2020-06-29 03:22:20', '2020-06-29 03:24:48', NULL, ''),
(26, 1, 'Banner', '', '_self', 'voyager-images', '#000000', 18, 4, '2020-06-30 00:12:20', '2020-06-30 00:50:03', 'voyager.banner.index', 'null'),
(27, 2, 'Categories', '', '_self', NULL, '#000000', NULL, 1, '2020-06-30 03:39:12', '2020-06-30 03:47:54', NULL, ''),
(28, 2, 'Sell Machines', '', '_self', NULL, '#000000', NULL, 2, '2020-06-30 03:40:08', '2020-06-30 03:47:56', NULL, ''),
(29, 2, 'About Us', 'about', '_self', NULL, '#000000', NULL, 3, '2020-06-30 03:41:39', '2020-06-30 04:04:44', NULL, ''),
(30, 2, 'Contact us', '', '_self', NULL, '#000000', NULL, 4, '2020-06-30 03:41:45', '2020-06-30 03:47:56', NULL, ''),
(31, 2, 'Help & Support', '', '_self', NULL, '#000000', NULL, 5, '2020-06-30 03:41:59', '2020-06-30 03:47:56', NULL, ''),
(32, 2, 'category1', '', '_self', NULL, '#000000', 27, 1, '2020-06-30 03:47:49', '2020-06-30 03:47:56', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2016_01_01_000000_create_pages_table', 2),
(25, '2016_01_01_000000_create_posts_table', 2),
(26, '2016_02_15_204651_create_categories_table', 2),
(27, '2017_04_11_000000_alter_post_nullable_fields_table', 2),
(28, '2020_06_27_101640_create_languages_table', 3),
(30, '2020_06_29_120200_create_banner_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 'Home', '32e4wr4et', '<p>dewfrt</p>', NULL, 'home', 'home', 'home', 'ACTIVE', '2020-06-30 00:47:36', '2020-06-30 04:21:22'),
(4, 1, 'About', 'about', '<div class=\"container margin_60\">\n<div class=\"main_title\">\n<h2>&nbsp;</h2>\n<h2>About MACKINAT</h2>\n<p>Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.</p>\n</div>\n<div class=\"row\">\n<div class=\"col-lg-12 wow fadeIn\" data-wow-delay=\"0.1s\">\n<p style=\"text-align: justify;\">orem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vestibulum ipsum in lectus interdum, vitae placerat eros placerat. Phasellus sapien sapien, gravida at nunc ac, mattis tempor sem. Suspendisse quis sem in eros condimentum fermentum vitae sit amet odio. Sed cursus ornare dui, vel pellentesque nunc viverra quis. Mauris non laoreet augue, quis aliquam tortor. Mauris tristique, turpis a congue efficitur, tortor orci blandit dui, eu vulputate libero augue nec arcu. Sed ullamcorper, est sit amet malesuada aliquam, ipsum mi pulvinar nunc, in hendrerit libero nibh nec nunc. Nam cursus eleifend ex a ultrices. Aliquam et turpis iaculis, varius ipsum viverra, blandit nisl. Proin sapien ante, efficitur vel sem sed, porta fermentum felis. Sed malesuada lorem et vestibulum dignissim. Mauris auctor, dolor eu faucibus viverra, ipsum purus facilisis leo, vitae scelerisque orci nisi et arcu.</p>\n<p style=\"text-align: justify;\">Pellentesque ornare, velit in porttitor mattis, risus erat vehicula est, sed hendrerit orci augue lobortis diam. Quisque facilisis lobortis tempor. Nam at eros vel felis gravida sollicitudin vitae in lacus. Curabitur orci nibh, tincidunt vitae erat et, tincidunt blandit elit. Praesent molestie mi odio, in tempus erat mollis et. Proin vitae maximus ante. In egestas orci sed metus eleifend iaculis. Proin quis gravida quam, vitae convallis mauris. Sed in nibh purus. Sed ultricies sapien faucibus laoreet interdum.</p>\n</div>\n</div>\n<div class=\"row\">&nbsp;</div>\n<div class=\"row\">\n<div class=\"col-lg-6 wow fadeIn\" data-wow-delay=\"0.1s\">\n<div class=\"feature\">\n<h3>+ 1000 Customers</h3>\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.</p>\n</div>\n</div>\n<div class=\"col-lg-6 wow fadeIn\" data-wow-delay=\"0.5s\">\n<div class=\"feature\">\n<h3>Accesibility managment</h3>\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.</p>\n</div>\n</div>\n</div>\n<!-- End row -->\n<div class=\"row\">\n<div class=\"col-lg-6 wow fadeIn\" data-wow-delay=\"0.3s\">\n<div class=\"feature\">\n<h3>H24 Support</h3>\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.</p>\n</div>\n</div>\n<div class=\"col-lg-6 wow fadeIn\" data-wow-delay=\"0.4s\">\n<div class=\"feature\">\n<h3>10 Languages available</h3>\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.</p>\n</div>\n</div>\n</div>\n<!-- End row -->\n<div class=\"row\">&nbsp;</div>\n</div>\n<!-- End container -->\n<div class=\"container-fluid\">\n<div class=\"row\">\n<div class=\"col-lg-6 nopadding features-intro-img\">\n<div class=\"features-bg\">\n<div class=\"features-img\">&nbsp;</div>\n</div>\n</div>\n<div class=\"col-lg-6 nopadding\">\n<div class=\"features-content\">\n<h3>\"Global company, Local Support!\"</h3>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>\n<p><a class=\" btn_1\" href=\"#\">Contact us</a></p>\n</div>\n</div>\n</div>\n</div>\n<!-- End container-fluid  -->\n<p>&nbsp;</p>', NULL, 'about', 'about', 'about', 'ACTIVE', '2020-06-30 04:39:48', '2020-06-30 05:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-06-27 04:39:23', '2020-06-27 04:39:23'),
(2, 'browse_bread', NULL, '2020-06-27 04:39:23', '2020-06-27 04:39:23'),
(3, 'browse_database', NULL, '2020-06-27 04:39:23', '2020-06-27 04:39:23'),
(4, 'browse_media', NULL, '2020-06-27 04:39:23', '2020-06-27 04:39:23'),
(5, 'browse_compass', NULL, '2020-06-27 04:39:23', '2020-06-27 04:39:23'),
(6, 'browse_menus', 'menus', '2020-06-27 04:39:23', '2020-06-27 04:39:23'),
(7, 'read_menus', 'menus', '2020-06-27 04:39:23', '2020-06-27 04:39:23'),
(8, 'edit_menus', 'menus', '2020-06-27 04:39:23', '2020-06-27 04:39:23'),
(9, 'add_menus', 'menus', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(10, 'delete_menus', 'menus', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(11, 'browse_roles', 'roles', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(12, 'read_roles', 'roles', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(13, 'edit_roles', 'roles', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(14, 'add_roles', 'roles', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(15, 'delete_roles', 'roles', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(16, 'browse_users', 'users', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(17, 'read_users', 'users', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(18, 'edit_users', 'users', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(19, 'add_users', 'users', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(20, 'delete_users', 'users', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(21, 'browse_settings', 'settings', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(22, 'read_settings', 'settings', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(23, 'edit_settings', 'settings', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(24, 'add_settings', 'settings', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(25, 'delete_settings', 'settings', '2020-06-27 04:39:24', '2020-06-27 04:39:24'),
(26, 'browse_categories', 'categories', '2020-06-27 04:39:34', '2020-06-27 04:39:34'),
(27, 'read_categories', 'categories', '2020-06-27 04:39:34', '2020-06-27 04:39:34'),
(28, 'edit_categories', 'categories', '2020-06-27 04:39:34', '2020-06-27 04:39:34'),
(29, 'add_categories', 'categories', '2020-06-27 04:39:34', '2020-06-27 04:39:34'),
(30, 'delete_categories', 'categories', '2020-06-27 04:39:34', '2020-06-27 04:39:34'),
(31, 'browse_posts', 'posts', '2020-06-27 04:39:36', '2020-06-27 04:39:36'),
(32, 'read_posts', 'posts', '2020-06-27 04:39:36', '2020-06-27 04:39:36'),
(33, 'edit_posts', 'posts', '2020-06-27 04:39:36', '2020-06-27 04:39:36'),
(34, 'add_posts', 'posts', '2020-06-27 04:39:36', '2020-06-27 04:39:36'),
(35, 'delete_posts', 'posts', '2020-06-27 04:39:36', '2020-06-27 04:39:36'),
(36, 'browse_pages', 'pages', '2020-06-27 04:39:37', '2020-06-27 04:39:37'),
(37, 'read_pages', 'pages', '2020-06-27 04:39:37', '2020-06-27 04:39:37'),
(38, 'edit_pages', 'pages', '2020-06-27 04:39:37', '2020-06-27 04:39:37'),
(39, 'add_pages', 'pages', '2020-06-27 04:39:37', '2020-06-27 04:39:37'),
(40, 'delete_pages', 'pages', '2020-06-27 04:39:37', '2020-06-27 04:39:37'),
(41, 'browse_hooks', NULL, '2020-06-27 04:39:41', '2020-06-27 04:39:41'),
(47, 'browse_languages', 'languages', '2020-06-27 05:15:36', '2020-06-27 05:15:36'),
(48, 'read_languages', 'languages', '2020-06-27 05:15:36', '2020-06-27 05:15:36'),
(49, 'edit_languages', 'languages', '2020-06-27 05:15:36', '2020-06-27 05:15:36'),
(50, 'add_languages', 'languages', '2020-06-27 05:15:36', '2020-06-27 05:15:36'),
(51, 'delete_languages', 'languages', '2020-06-27 05:15:36', '2020-06-27 05:15:36'),
(87, 'browse_banner', 'banner', '2020-06-30 00:12:20', '2020-06-30 00:12:20'),
(88, 'read_banner', 'banner', '2020-06-30 00:12:20', '2020-06-30 00:12:20'),
(89, 'edit_banner', 'banner', '2020-06-30 00:12:20', '2020-06-30 00:12:20'),
(90, 'add_banner', 'banner', '2020-06-30 00:12:20', '2020-06-30 00:12:20'),
(91, 'delete_banner', 'banner', '2020-06-30 00:12:20', '2020-06-30 00:12:20');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-06-27 04:39:36', '2020-06-27 04:39:36'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-06-27 04:39:36', '2020-06-27 04:39:36'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-06-27 04:39:36', '2020-06-27 04:39:36'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-06-27 04:39:36', '2020-06-27 04:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-06-27 04:39:23', '2020-06-27 04:39:23'),
(2, 'user', 'Normal User', '2020-06-27 04:39:23', '2020-06-27 04:39:23');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-06-27 04:39:38', '2020-06-27 04:39:38'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-06-27 04:39:38', '2020-06-27 04:39:38'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-06-27 04:39:38', '2020-06-27 04:39:38'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-06-27 04:39:38', '2020-06-27 04:39:38'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-06-27 04:39:38', '2020-06-27 04:39:38'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-06-27 04:39:38', '2020-06-27 04:39:38'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-06-27 04:39:38', '2020-06-27 04:39:38'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-06-27 04:39:38', '2020-06-27 04:39:38'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-06-27 04:39:38', '2020-06-27 04:39:38'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-06-27 04:39:38', '2020-06-27 04:39:38'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-06-27 04:39:39', '2020-06-27 04:39:39'),
(43, 'menu_items', 'title', 18, 'ar', 'CMS', '2020-06-29 03:23:52', '2020-06-29 03:23:52'),
(44, 'menu_items', 'title', 18, 'fr', 'CMS', '2020-06-29 03:23:52', '2020-06-29 03:23:52'),
(45, 'menu_items', 'title', 18, 'de', 'CMS', '2020-06-29 03:23:52', '2020-06-29 03:23:52'),
(46, 'menu_items', 'title', 18, 'it', 'CMS', '2020-06-29 03:23:52', '2020-06-29 03:23:52'),
(47, 'menu_items', 'title', 17, 'ar', 'Languages', '2020-06-29 04:41:16', '2020-06-29 04:41:16'),
(48, 'menu_items', 'title', 17, 'fr', 'Languages', '2020-06-29 04:41:16', '2020-06-29 04:41:16'),
(49, 'menu_items', 'title', 17, 'de', 'Languages', '2020-06-29 04:41:16', '2020-06-29 04:41:16'),
(50, 'menu_items', 'title', 17, 'it', 'Languages', '2020-06-29 04:41:17', '2020-06-29 04:41:17'),
(51, 'menu_items', 'title', 1, 'ar', 'Dashboard', '2020-06-29 04:42:07', '2020-06-29 04:42:07'),
(52, 'menu_items', 'title', 1, 'fr', 'Dashboard', '2020-06-29 04:42:07', '2020-06-29 04:42:07'),
(53, 'menu_items', 'title', 1, 'de', 'Dashboard', '2020-06-29 04:42:07', '2020-06-29 04:42:07'),
(54, 'menu_items', 'title', 1, 'it', 'Dashboard', '2020-06-29 04:42:07', '2020-06-29 04:42:07'),
(75, 'menu_items', 'title', 22, 'ar', 'Banner', '2020-06-29 23:07:31', '2020-06-29 23:07:31'),
(76, 'menu_items', 'title', 22, 'fr', 'Banner', '2020-06-29 23:07:32', '2020-06-29 23:07:32'),
(77, 'menu_items', 'title', 22, 'de', 'Banner', '2020-06-29 23:07:33', '2020-06-29 23:07:33'),
(78, 'menu_items', 'title', 22, 'it', 'Banner', '2020-06-29 23:07:33', '2020-06-29 23:07:33'),
(87, 'data_rows', 'display_name', 90, 'ar', 'Id', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(88, 'data_rows', 'display_name', 90, 'fr', 'Id', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(89, 'data_rows', 'display_name', 90, 'de', 'Id', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(90, 'data_rows', 'display_name', 90, 'it', 'Id', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(91, 'data_rows', 'display_name', 91, 'ar', 'PageId', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(92, 'data_rows', 'display_name', 91, 'fr', 'PageId', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(93, 'data_rows', 'display_name', 91, 'de', 'PageId', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(94, 'data_rows', 'display_name', 91, 'it', 'PageId', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(95, 'data_rows', 'display_name', 92, 'ar', 'Title', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(96, 'data_rows', 'display_name', 92, 'fr', 'Title', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(97, 'data_rows', 'display_name', 92, 'de', 'Title', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(98, 'data_rows', 'display_name', 92, 'it', 'Title', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(99, 'data_rows', 'display_name', 93, 'ar', 'Description', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(100, 'data_rows', 'display_name', 93, 'fr', 'Description', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(101, 'data_rows', 'display_name', 93, 'de', 'Description', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(102, 'data_rows', 'display_name', 93, 'it', 'Description', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(103, 'data_rows', 'display_name', 94, 'ar', 'Image', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(104, 'data_rows', 'display_name', 94, 'fr', 'Image', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(105, 'data_rows', 'display_name', 94, 'de', 'Image', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(106, 'data_rows', 'display_name', 94, 'it', 'Image', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(107, 'data_rows', 'display_name', 95, 'ar', 'Status', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(108, 'data_rows', 'display_name', 95, 'fr', 'Status', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(109, 'data_rows', 'display_name', 95, 'de', 'Status', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(110, 'data_rows', 'display_name', 95, 'it', 'Status', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(111, 'data_rows', 'display_name', 96, 'ar', 'Created At', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(112, 'data_rows', 'display_name', 96, 'fr', 'Created At', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(113, 'data_rows', 'display_name', 96, 'de', 'Created At', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(114, 'data_rows', 'display_name', 96, 'it', 'Created At', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(115, 'data_rows', 'display_name', 97, 'ar', 'Updated At', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(116, 'data_rows', 'display_name', 97, 'fr', 'Updated At', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(117, 'data_rows', 'display_name', 97, 'de', 'Updated At', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(118, 'data_rows', 'display_name', 97, 'it', 'Updated At', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(119, 'data_rows', 'display_name', 98, 'ar', 'pages', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(120, 'data_rows', 'display_name', 98, 'fr', 'pages', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(121, 'data_rows', 'display_name', 98, 'de', 'pages', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(122, 'data_rows', 'display_name', 98, 'it', 'pages', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(123, 'data_types', 'display_name_singular', 24, 'ar', 'Banner', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(124, 'data_types', 'display_name_singular', 24, 'fr', 'Banner', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(125, 'data_types', 'display_name_singular', 24, 'de', 'Banner', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(126, 'data_types', 'display_name_singular', 24, 'it', 'Banner', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(127, 'data_types', 'display_name_plural', 24, 'ar', 'Banner', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(128, 'data_types', 'display_name_plural', 24, 'fr', 'Banner', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(129, 'data_types', 'display_name_plural', 24, 'de', 'Banner', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(130, 'data_types', 'display_name_plural', 24, 'it', 'Banner', '2020-06-30 00:19:26', '2020-06-30 00:19:26'),
(131, 'data_rows', 'display_name', 99, 'ar', 'pages', '2020-06-30 00:42:38', '2020-06-30 00:42:38'),
(132, 'data_rows', 'display_name', 99, 'fr', 'pages', '2020-06-30 00:42:38', '2020-06-30 00:42:38'),
(133, 'data_rows', 'display_name', 99, 'de', 'pages', '2020-06-30 00:42:38', '2020-06-30 00:42:38'),
(134, 'data_rows', 'display_name', 99, 'it', 'pages', '2020-06-30 00:42:38', '2020-06-30 00:42:38'),
(135, 'menu_items', 'title', 26, 'ar', 'Banner', '2020-06-30 00:49:53', '2020-06-30 00:49:53'),
(136, 'menu_items', 'title', 26, 'fr', 'Banner', '2020-06-30 00:49:53', '2020-06-30 00:49:53'),
(137, 'menu_items', 'title', 26, 'de', 'Banner', '2020-06-30 00:49:53', '2020-06-30 00:49:53'),
(138, 'menu_items', 'title', 26, 'it', 'Banner', '2020-06-30 00:49:53', '2020-06-30 00:49:53'),
(147, 'pages', 'title', 2, 'ar', 'الصفحة الرئيسية', '2020-06-30 01:20:32', '2020-06-30 01:20:32'),
(148, 'pages', 'title', 2, 'fr', 'Home', '2020-06-30 01:20:32', '2020-06-30 01:20:32'),
(149, 'pages', 'title', 2, 'de', 'Home', '2020-06-30 01:20:32', '2020-06-30 01:20:32'),
(150, 'pages', 'title', 2, 'it', 'Home', '2020-06-30 01:20:32', '2020-06-30 01:20:32'),
(151, 'pages', 'body', 2, 'ar', '<p>حول</p>', '2020-06-30 01:20:32', '2020-06-30 04:18:21'),
(152, 'pages', 'body', 2, 'fr', '<p>ewrt</p>', '2020-06-30 01:20:32', '2020-06-30 01:20:32'),
(153, 'pages', 'body', 2, 'de', '<p>ewrt</p>', '2020-06-30 01:20:32', '2020-06-30 01:20:32'),
(154, 'pages', 'body', 2, 'it', '<p>ewrt</p>', '2020-06-30 01:20:32', '2020-06-30 01:20:32'),
(155, 'pages', 'slug', 2, 'ar', 'home', '2020-06-30 01:20:32', '2020-06-30 01:20:32'),
(156, 'pages', 'slug', 2, 'fr', 'home', '2020-06-30 01:20:32', '2020-06-30 01:20:32'),
(157, 'pages', 'slug', 2, 'de', 'home', '2020-06-30 01:20:32', '2020-06-30 01:20:32'),
(158, 'pages', 'slug', 2, 'it', 'home', '2020-06-30 01:20:32', '2020-06-30 01:20:32'),
(167, 'banner', 'title', 6, 'ar', 'محرك بحث للآلات!', '2020-06-30 02:48:12', '2020-06-30 02:49:03'),
(168, 'banner', 'title', 6, 'fr', 'SEARCH ENGINE FOR MACHINES!', '2020-06-30 02:48:12', '2020-06-30 02:48:12'),
(169, 'banner', 'title', 6, 'de', 'SEARCH ENGINE FOR MACHINES!', '2020-06-30 02:48:12', '2020-06-30 02:48:12'),
(170, 'banner', 'title', 6, 'it', 'SEARCH ENGINE FOR MACHINES!', '2020-06-30 02:48:12', '2020-06-30 02:48:12'),
(171, 'banner', 'description', 6, 'ar', 'اكتشف الماكينات حول العالم', '2020-06-30 02:48:12', '2020-06-30 02:49:03'),
(172, 'banner', 'description', 6, 'fr', 'DISCOVER THE MACHINES AROUND THE WORLD', '2020-06-30 02:48:13', '2020-06-30 02:48:13'),
(173, 'banner', 'description', 6, 'de', 'DISCOVER THE MACHINES AROUND THE WORLD', '2020-06-30 02:48:13', '2020-06-30 02:48:13'),
(174, 'banner', 'description', 6, 'it', 'DISCOVER THE MACHINES AROUND THE WORLD', '2020-06-30 02:48:13', '2020-06-30 02:48:13'),
(175, 'menu_items', 'title', 29, 'ar', 'About Us', '2020-06-30 04:04:44', '2020-06-30 04:04:44'),
(176, 'menu_items', 'title', 29, 'fr', 'About Us', '2020-06-30 04:04:44', '2020-06-30 04:04:44'),
(177, 'menu_items', 'title', 29, 'de', 'About Us', '2020-06-30 04:04:44', '2020-06-30 04:04:44'),
(178, 'menu_items', 'title', 29, 'it', 'About Us', '2020-06-30 04:04:44', '2020-06-30 04:04:44'),
(179, 'pages', 'title', 4, 'ar', 'About', '2020-06-30 04:40:43', '2020-06-30 04:40:43'),
(180, 'pages', 'title', 4, 'fr', 'About', '2020-06-30 04:40:43', '2020-06-30 04:40:43'),
(181, 'pages', 'title', 4, 'de', 'About', '2020-06-30 04:40:43', '2020-06-30 04:40:43'),
(182, 'pages', 'title', 4, 'it', 'About', '2020-06-30 04:40:43', '2020-06-30 04:40:43'),
(183, 'pages', 'slug', 4, 'ar', 'about', '2020-06-30 04:40:43', '2020-06-30 04:40:43'),
(184, 'pages', 'slug', 4, 'fr', 'about', '2020-06-30 04:40:43', '2020-06-30 04:40:43'),
(185, 'pages', 'slug', 4, 'de', 'about', '2020-06-30 04:40:43', '2020-06-30 04:40:43'),
(186, 'pages', 'slug', 4, 'it', 'about', '2020-06-30 04:40:43', '2020-06-30 04:40:43'),
(187, 'pages', 'body', 4, 'ar', '<div class=\"container margin_60\">\n<div class=\"main_title\">\n<h2>&nbsp;</h2>\n<h2>معلومات عن ماكينات</h2>\n<p>Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.</p>\n</div>\n<div class=\"row\">\n<div class=\"col-lg-12 wow fadeIn\" data-wow-delay=\"0.1s\">\n<p style=\"text-align: justify;\">orem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vestibulum ipsum in lectus interdum, vitae placerat eros placerat. Phasellus sapien sapien, gravida at nunc ac, mattis tempor sem. Suspendisse quis sem in eros condimentum fermentum vitae sit amet odio. Sed cursus ornare dui, vel pellentesque nunc viverra quis. Mauris non laoreet augue, quis aliquam tortor. Mauris tristique, turpis a congue efficitur, tortor orci blandit dui, eu vulputate libero augue nec arcu. Sed ullamcorper, est sit amet malesuada aliquam, ipsum mi pulvinar nunc, in hendrerit libero nibh nec nunc. Nam cursus eleifend ex a ultrices. Aliquam et turpis iaculis, varius ipsum viverra, blandit nisl. Proin sapien ante, efficitur vel sem sed, porta fermentum felis. Sed malesuada lorem et vestibulum dignissim. Mauris auctor, dolor eu faucibus viverra, ipsum purus facilisis leo, vitae scelerisque orci nisi et arcu.</p>\n<p style=\"text-align: justify;\">Pellentesque ornare, velit in porttitor mattis, risus erat vehicula est, sed hendrerit orci augue lobortis diam. Quisque facilisis lobortis tempor. Nam at eros vel felis gravida sollicitudin vitae in lacus. Curabitur orci nibh, tincidunt vitae erat et, tincidunt blandit elit. Praesent molestie mi odio, in tempus erat mollis et. Proin vitae maximus ante. In egestas orci sed metus eleifend iaculis. Proin quis gravida quam, vitae convallis mauris. Sed in nibh purus. Sed ultricies sapien faucibus laoreet interdum.</p>\n</div>\n</div>\n<div class=\"row\">&nbsp;</div>\n<div class=\"row\">\n<div class=\"col-lg-6 wow fadeIn\" data-wow-delay=\"0.1s\">\n<div class=\"feature\">\n<h3>+ 1000 Customers</h3>\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.</p>\n</div>\n</div>\n<div class=\"col-lg-6 wow fadeIn\" data-wow-delay=\"0.5s\">\n<div class=\"feature\">\n<h3>Accesibility managment</h3>\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.</p>\n</div>\n</div>\n</div>\n<!-- End row -->\n<div class=\"row\">\n<div class=\"col-lg-6 wow fadeIn\" data-wow-delay=\"0.3s\">\n<div class=\"feature\">\n<h3>دعم H24</h3>\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.</p>\n</div>\n</div>\n<div class=\"col-lg-6 wow fadeIn\" data-wow-delay=\"0.4s\">\n<div class=\"feature\">\n<h3>10 لغات متاحة</h3>\n<p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.</p>\n</div>\n</div>\n</div>\n<!-- End row -->\n<div class=\"row\">&nbsp;</div>\n</div>\n<!-- End container -->', '2020-06-30 04:42:45', '2020-06-30 05:11:00'),
(188, 'pages', 'body', 4, 'fr', '<p>ASERGTHYJU</p>', '2020-06-30 04:42:45', '2020-06-30 04:42:45'),
(189, 'pages', 'body', 4, 'de', '<p>ASERGTHYJU</p>', '2020-06-30 04:42:45', '2020-06-30 04:42:45'),
(190, 'pages', 'body', 4, 'it', '<p>ASERGTHYJU</p>', '2020-06-30 04:42:45', '2020-06-30 04:42:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$81GXGQ49VsqriHPC14.76.yKVjJdazE9QQMI6rz6lpZZ4SVHzFmcq', 'CkA86qlXXM7aHQ3JfnihY11IwZRObfQl94sOQxbt5T2wvLXOvQEYUSvMrtHQ', '{\"locale\":\"en\"}', '2020-06-27 04:39:34', '2020-06-30 01:22:49');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
